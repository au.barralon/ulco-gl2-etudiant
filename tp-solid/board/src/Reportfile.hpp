#pragma once

#include "Itemable.hpp"

#include <fstream>
#include <iostream>

class Reportfile{
    private :
        std::ofstream _ofs;

    public :
        Reportfile(){};

        void reportFile(const Itemable& item) {
            for (const std::string & item : item.getItems())
                _ofs << item << std::endl;
            _ofs << std::endl;
        }
}