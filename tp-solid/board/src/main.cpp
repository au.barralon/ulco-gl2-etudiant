
#include "Board.hpp"
#include "ReportStdout.hpp"

void testBoard(Board & b) {
    ReportStdout rs;
    std::cout << b.getTitle() << std::endl;
    b.add("item 1");
    b.add("item 2");
    rs.reportStdout(b);
}

int main() {
    Board b1("test1.txt");
    testBoard(b1);
    return 0;
}

