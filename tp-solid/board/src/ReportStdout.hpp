#pragma once

#include "Itemable.hpp"

#include <fstream>
#include <iostream>

class ReportStdout{
        public : 
            ReportStdout(){};

            void reportStdout(const Itemable & item) {
            for (const std::string & item : item.getItems())
                std::cout << item << std::endl;
            std::cout << std::endl;
        }
}